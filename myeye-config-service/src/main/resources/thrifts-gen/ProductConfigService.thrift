namespace java com.freedom.monitor.myeye.config.service


struct Product{
  1: i64 id,
  2: string pid,
  3: string pname,
  4: string pcode,
  5: string pdesc,
  6: string createTime,
  7: string email,
  8: string mobile,
  9: string createBy
}

struct ProductResult{
  1: list<Product> productList,
  2: i32 total,
  3: bool succeed,
  4: string errorMsg
}



service ProductConfigService {

 ProductResult queryProduct(1:i32 page,2:i32 rows,3:string operatorId) 
 list<Product> queryProductByOperatorId(1:string userId)
 bool          addProduct(1:string pname,2:string pcode,3:string pdesc,4:string email,5:string mobile,6:string createBy)
 bool          editProduct(1:string pname,2:string pcode,3:string pdesc,4:string email,5:string mobile,6: string pid)
 bool          deleteProduct(1:string pid)
 

}

