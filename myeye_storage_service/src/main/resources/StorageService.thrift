namespace java com.freedom.monitor.myeye.storage.service

struct ServiceResult{
 1: bool succeed,
 2: string errorMsg,
 3: list<string> serviceList
}

struct ServerIdResult{
1:bool succeed,
2:string errorMsg,
3:list<string> serverIdList
}

service StorageService {

 bool put(1:string data)
 ServiceResult getService(1:string productCode)
 ServerIdResult getServerId(1:string productCode,2:string serviceCode)
 
}

