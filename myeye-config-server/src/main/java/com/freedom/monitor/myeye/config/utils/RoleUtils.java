package com.freedom.monitor.myeye.config.utils;

import org.apache.log4j.Logger;

public class RoleUtils {
	//
	private static final Logger logger = Logger.getLogger(RoleUtils.class);
	public static String[] desc = { "", // 0
			"超级管理员", // 1
			"应用管理员", // 2
			"", // 3
			"普通管理员", // 4
			"", // 5
			"", // 6
			"", // 7
			"普通用户",// 8

	};

	public static String getRoleByInt(int r) {
		try {
			String str = new String(desc[r].getBytes("UTF-8"));
			logger.debug("str--->" + str);
			return str;
			// return desc[r];
		} catch (Exception e) {
			return "";
		}
	}
}
