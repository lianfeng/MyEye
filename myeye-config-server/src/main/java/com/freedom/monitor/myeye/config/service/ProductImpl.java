package com.freedom.monitor.myeye.config.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.thrift.TException;

import com.freedom.monitor.myeye.config.model.Count;
import com.freedom.monitor.myeye.config.utils.DbUtilsHelper;
import com.freedom.rpc.thrift.common.utils.Logger;

//用户服务，比如登录
public class ProductImpl implements ProductConfigService.Iface {
	private static final Logger logger = Logger.getLogger(ProductImpl.class);

	


	@SuppressWarnings("unchecked")
	@Override
	public ProductResult queryProduct(int page, int rows, String operatorId) throws TException {
		logger.debug("ConfigServiceImpl.queryProduct is invoked...");
		ProductResult productResult = new ProductResult();
		productResult.setSucceed(true);
		// 1)构造sql
		String sql = "select distinct id,pid,pname,pcode,pdesc,DATE_FORMAT(createtime,'%Y-%m-%d %k:%i:%s') as createTime,email,mobile,createBy from t_system_product ";
		{
			// 目前的策略是只能看到自己创建的Product
			sql += " where createBy=?";
		}
		sql += " order by createTime desc ";
		sql += " limit ?,?";
		logger.info("sql ---> " + sql);
		List<com.freedom.monitor.myeye.config.model.Product> products = (List<com.freedom.monitor.myeye.config.model.Product>) DbUtilsHelper
				.queryManyObject(sql, com.freedom.monitor.myeye.config.model.Product.class, operatorId,
						(page - 1) * rows, rows);
		if (null == products) {// 修正一下
			productResult.setSucceed(false);
			return productResult;
		}
		logger.debug("products--->" + products);
		for (com.freedom.monitor.myeye.config.model.Product product : products) {
			com.freedom.monitor.myeye.config.service.Product rpcProduct = new com.freedom.monitor.myeye.config.service.Product();
			rpcProduct.setId(product.getId());
			rpcProduct.setPid(product.getPid());
			rpcProduct.setPname(product.getPname());
			rpcProduct.setPcode(product.getPcode());
			rpcProduct.setPdesc(product.getPdesc());
			rpcProduct.setCreateTime(product.getCreateTime());
			rpcProduct.setEmail(product.getEmail());
			rpcProduct.setMobile(product.getMobile());
			rpcProduct.setCreateBy(product.getCreateBy());
			productResult.addToProductList(rpcProduct);
		}

		// 2)设置总数
		sql = "select count(*) as count from t_system_product ";
		sql += " where createBy=?";
		Count count = (Count) DbUtilsHelper.queryOneObject(sql, Count.class, operatorId);
		if (null == count) {
			productResult.setSucceed(false);
			return productResult;
		}
		productResult.setTotal(count.getCount());
		// 5)返回结果
		return productResult;
	}

	@Override
	public boolean addProduct(String pname, String pcode, String pdesc, String email, String mobile, String createBy)
			throws TException {
		// 新增Product
		logger.debug("pname---" + pname);
		try {
			// 1)构造sql
			String sql = "insert into t_system_product(pid,pname,pcode,pdesc,email,mobile,createBy) values(?,?,?,?,?,?,?) ";
			// 2)执行sql
			int result = DbUtilsHelper.insertOneObject(sql, RandomStringUtils.random(64, true, true), pname, pcode,
					pdesc, email, mobile, createBy);
			return (1 == result);
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean editProduct(String pname, String pcode, String pdesc, String email, String mobile, String pid)
			throws TException {
		// 修改product
		logger.debug("ConfigServiceImpl.editProduct invoked...");
		try {
			String sql = "update t_system_product set pname=?,pcode=?,pdesc=?,email=?,mobile=? where pid=?";
			// 2)执行sql
			int result = DbUtilsHelper.updateOneObject(sql, pname, pcode, pdesc, email, mobile, pid);
			return (1 == result);
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean deleteProduct(String pid) throws TException {
		// 1)构造sql
		String sql = "delete from t_system_product where pid=?";
		// 2)执行sql
		int result = DbUtilsHelper.deleteOneObject(sql, pid);
		// 3)返回结果
		return (1 == result);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Product> queryProductByOperatorId(String operatorId) throws TException {
		logger.debug("ConfigServiceImpl.queryProductByOperatorId is invoked...");
		// 1)构造sql
		String sql = "select distinct id,pid,pname,pcode,pdesc,DATE_FORMAT(createtime,'%Y-%m-%d %k:%i:%s') as createTime,email,mobile,createBy from t_system_product ";
		{ // 目前的策略是只能看到自己创建的Product,其实还应该是组内授予[普通管理员的成员]
			sql += " where createBy=?";
		}
		sql += " order by createTime desc ";
		logger.info("sql ---> " + sql);
		List<com.freedom.monitor.myeye.config.model.Product> products = (List<com.freedom.monitor.myeye.config.model.Product>) DbUtilsHelper
				.queryManyObject(sql, com.freedom.monitor.myeye.config.model.Product.class, operatorId);
		if (null == products) {// 修正一下
			return new ArrayList<Product>();
		}
		logger.debug("products--->" + products);
		List<Product> productResult = new ArrayList<Product>();
		for (com.freedom.monitor.myeye.config.model.Product product : products) {
			com.freedom.monitor.myeye.config.service.Product rpcProduct = new com.freedom.monitor.myeye.config.service.Product();
			rpcProduct.setId(product.getId());
			rpcProduct.setPid(product.getPid());
			rpcProduct.setPname(product.getPname());
			rpcProduct.setPcode(product.getPcode());
			rpcProduct.setPdesc(product.getPdesc());
			rpcProduct.setCreateTime(product.getCreateTime());
			rpcProduct.setEmail(product.getEmail());
			rpcProduct.setMobile(product.getMobile());
			rpcProduct.setCreateBy(product.getCreateBy());
			productResult.add(rpcProduct);
		}
		// 3)返回结果
		return productResult;
	}


}
