package com.freedom.monitor.myeye.config.utils;

public class SqlUtils {
	// 查询系统用户
	public static String QUERY_SYSTEM_USER = "select distinct id,userid,"//
			+ "username,"//
			+ "pwd,"//
			+ "role,"//
			// + "DATE_FORMAT(createtime,'%Y-%m-%d %k:%i:%s') createtime,"//
			+ "createtime,"//
			+ "token "//
			+ " from t_system_user where "//
			+ " username='%s' and pwd='%s' limit 1";//
	//
	// 更新系统用户的token
	public static String UPDATE_SYSTEM_USER = "update t_system_user "//
			+ " set token=? where userid=?";//
	//
	//
	//
	// 查询普通用户
	public static String QUERY_COMMON_USER = "select distinct id,userid,"//
			+ "username,"//
			+ "pwd,"//
			+ "role,"//
			// + "DATE_FORMAT(createtime,'%Y-%m-%d %k:%i:%s') createtime,"//
			+ "createtime,"//
			+ "token "//
			+ " from t_system_group_member where "//
			+ " username='%s' and pwd='%s' limit 1";//
	// 更新系统用户的token
	public static String UPDATE_COMMON_USER = "update t_system_group_member "//
			+ " set token=? where userid=?";//
}
