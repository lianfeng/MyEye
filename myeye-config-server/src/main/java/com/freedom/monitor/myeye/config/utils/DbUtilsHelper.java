package com.freedom.monitor.myeye.config.utils;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import com.freedom.rpc.thrift.common.utils.Logger;

public class DbUtilsHelper {
	private static final Logger logger = Logger.getLogger(DbUtilsHelper.class);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object queryOneObject(String sql, Class clz) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			QueryRunner qr = new QueryRunner();
			return qr.query(pooledConnection, sql, new BeanHandler(clz));
		} catch (Exception e) {
			logger.error(e.toString());
			return null;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.closeQuietly(pooledConnection);// 只是查询，不需要提交事务
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object queryOneObject(String sql, Class clz, Object... params) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			QueryRunner qr = new QueryRunner();
			return qr.query(pooledConnection, sql, new BeanHandler(clz), params);
		} catch (Exception e) {
			logger.error(e.toString());
			return null;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.closeQuietly(pooledConnection);// 只是查询，不需要提交事务
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object queryManyObject(String sql, Class clz, Object... params) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			QueryRunner qr = new QueryRunner();
			return qr.query(pooledConnection, sql, new BeanListHandler(clz), params);// 使用了BeanList
		} catch (Exception e) {
			logger.error(e.toString());
			return null;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.closeQuietly(pooledConnection);// 只是查询，不需要提交事务
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object queryManyObject(String sql, Class clz) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			QueryRunner qr = new QueryRunner();
			return qr.query(pooledConnection, sql, new BeanListHandler(clz));// 使用了BeanList
		} catch (Exception e) {
			logger.error(e.toString());
			return null;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.closeQuietly(pooledConnection);// 只是查询，不需要提交事务
			}
		}
	}

	public static int insertOneObject(String sql, Object... params) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			QueryRunner qr = new QueryRunner();
			return qr.update(pooledConnection, sql, params);// 可以执行删除的!
		} catch (Exception e) {
			logger.error(e.toString());
			return 0;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.commitAndCloseQuietly(pooledConnection);// 需要提交事务
			}
		}
	}

	public static int updateOneObject(String sql, Object... params) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			QueryRunner qr = new QueryRunner();
			return qr.update(pooledConnection, sql, params);
		} catch (Exception e) {
			logger.error(e.toString());
			return 0;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.commitAndCloseQuietly(pooledConnection);// 需要提交事务
			}
		}
	}

	public static int deleteOneObject(String sql, Object... params) {
		DruidPooledConnection pooledConnection = null;
		try {
			DruidDataSource druid = DruidUtils.getInstance();
			pooledConnection = druid.getConnection();
			logger.debug("get connnection:" + pooledConnection);
			QueryRunner qr = new QueryRunner();
			int deleteCount = qr.update(pooledConnection, sql, params);// 可以执行删除的!
			logger.debug("deleteCount---"+deleteCount);
			return deleteCount;
		} catch (Exception e) {
			logger.error(e.toString());
			return 0;
		} finally {
			// 释放资源
			if (null != pooledConnection) {
				DbUtils.commitAndCloseQuietly(pooledConnection);// 需要提交事务
			}
		}
	}
}
