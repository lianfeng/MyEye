<%@ page language="java" session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.freedom.monitor.utils.ConstantUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="width:100%;height:100%;overflow:hidden">
<!-- 界面布局参考:http://www.jeasyui.com/demo/main/index.php?plugin=Layout&theme=default&dir=ltr&pitem= -->
<!-- 按钮参考: http://ecomfe.github.io/esf/demo/ -->
<!-- 点击按钮行为:http://www.jeasyui.com/demo/main/index.php?plugin=Tabs&theme=default&dir=ltr&pitem= -->
<!-- 用户管理: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/complextoolbar.html
file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/clientpagination.html
file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/multisorting.html
 -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<title>主界面</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/demo/demo.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/esf/esf.css">

<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.easyui.min.js"></script>

</head>

<body style="padding: 0px 0px 0px 0px; margin: 1px 1px 1px 1px;">
	<h2 style="display: none;">统一监控</h2>
	<p style="display: none;">欢迎使用</p>
	<div style="margin: 0px 0;"></div>
	<div class="easyui-layout"
		style="width: 100%; height: 620px; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;">
		<!-- <div data-options="region:'north'" style="height: 1px"></div> 
		<div data-options="region:'south',split:true" style="height: 1px;">			
		</div>
		-->
		<div id="eastRegion" data-options="region:'east',split:false"
			title="时间表盘" style="width: 100px;">
			<a href="#" class="easyui-linkbutton" onclick="logout()">退出</a>
			<span id="timeSpan"></span>
			
				
		</div>
		
		<div data-options="region:'west',split:false" title="菜单"
			style="width: 150px;">
			<div class="easyui-accordion" data-options="fit:true,border:false">
			
			<%		
			
				int role=(Integer)request.getAttribute("role");
			    if(1==role){//系统管理员
			     %>
			    	<div title="用户管理" data-options="selected:false"
							style="padding: 10px;">
							<!-- 每个按钮,一般用来对应用管理员进行管理 -->
							<button onclick="addPanel('用户管理','userManagementTab')"
								type="button" class="ui-button ui-button-fluid">
								<span class="ui-icon-users"></span>用户管理
							</button>
						</div>
						
						<div title="服务模板管理" data-options="selected:false"
							style="padding: 10px;">
							<button onclick="addPanel('服务模板管理','serviceTemplateManagementTab')"
								type="button" class="ui-button ui-button-fluid">
								<span class="ui-icon-users"></span>服务模板管理
							</button>
						</div>
					<%
			    }
			
			
				
				
			   if(2==role||4==role){//应用管理员,普通管理员
				%>
				<div title="组员管理" data-options="selected:false"
					style="padding: 10px;">
					<!-- 每个按钮,一般是应用管理员对普通管理员和普通用户进行管理 -->
					<button onclick="addPanel('组员管理','groupMemberManagementTab')"
						type="button" class="ui-button ui-button-fluid">
						<span class="ui-icon-users"></span>组员管理
					</button>
				</div>
				
				<div title="产品管理" style="padding: 10px">
					<!-- 应用管理员负责添加产品 -->
					<button onclick="addPanel('产品管理','productManagementTab')"
						type="button" class="ui-button ui-button-fluid">
						<span class="ui-icon-users"></span>产品管理
					</button>
				</div>
				
				
				<div title="服务参数定制" data-options="selected:false" style='padding:10px;'>
					<button
						onclick="addPanel('服务参数定制','productServiceManagementTab')"
						type="button" class="ui-button ui-button-fluid">
						<span class="ui-icon-users"></span>服务参数定制
					</button>
				</div>
				<%
			   }
			   
			   
			   
			
			   if(1==role||2==role||4==role||8==role){//所有人都可以查看面板
				%>
				<div title="数据面板" data-options="selected:false"
					style="padding: 10px;">
					<button onclick="addPanel('数据面板','dataPanelTab')" type="button"
						class="ui-button ui-button-fluid">
						<span class="ui-icon-users"></span>数据面板
					</button>
				</div>
				<%
			   }
			   
			%>
				
			</div>
		</div>
		<div data-options="region:'center',title:'统一监控中心',iconCls:'icon-ok'">
			<!-- 原有的屏蔽掉
			<div class="easyui-tabs"
				data-options="fit:true,border:false,plain:true">	
				<div title="DataGrid" style="padding: 5px">
					<table class="easyui-datagrid"
						data-options="url:'datagrid_data1.json',method:'get',singleSelect:true,fit:true,fitColumns:true">
						<thead>
							<tr>
								<th data-options="field:'itemid'" width="80">Item ID</th>
								<th data-options="field:'productid'" width="100">Product ID</th>
								<th data-options="field:'listprice',align:'right'" width="80">List
									Price</th>
								<th data-options="field:'unitcost',align:'right'" width="80">Unit
									Cost</th>
								<th data-options="field:'attr1'" width="150">Attribute</th>
								<th data-options="field:'status',align:'center'" width="50">Status</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			-->
			<div id="tt" class="easyui-tabs" fit="true"
				data-options="tools:'#tab-tools'" style="width: 100%; height: 100%"></div>


		</div>
	</div>

	<script type="text/javascript">
		var index = 0;
		function addPanel(tabTitle, id) {
			var exist = $('#tt').tabs('exists', tabTitle);
			if (exist) {
				$('#tt').tabs('select', tabTitle);
			} else {
				var iframeId = "";
				var iframeUrl = "";
				if ('userManagementTab' == id) {//用户管理
					iframeId = "userManagementIFrame";
					iframeUrl = "<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/user/index";
				}
				else if ('productManagementTab' == id) {//产品管理
					iframeId = "productManagementIFrame";
					iframeUrl = "<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/product/index";
				}
				else if ('groupMemberManagementTab' == id) {//组内成员管理
					iframeId = "groupMemberManagementIFrame";
					iframeUrl = "<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/member/index";
				}else if ('serviceTemplateManagementTab' == id) {//服务模板管理
					iframeId = "serviceTemplateManagementIFrame";
					iframeUrl = "<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/serviceTemplate/index";
				}else if ('productServiceManagementTab' == id) {//产品服务参数管理
					iframeId = "productServiceManagementIFrame";
					iframeUrl = "<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/productService/index";
				}else if('dataPanelTab'==id){//数据面板
					iframeId="dataPanelIFrame";
					iframeUrl="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/data/index";
				}
				
				var contentHtml = "";
				contentHtml += "<iframe width='100%' height='100%' id='";
				contentHtml += "" + iframeId;
				contentHtml += "" + "' frameborder='0' scrolling='auto' src='";
				contentHtml += iframeUrl;
				contentHtml += "'></iframe>";
				//开始填充内容
				$('#tt').tabs('add', {
					title : '' + tabTitle,
					content : contentHtml,
					closable : true
				});
			}
		}
		function removePanel() {
			var tab = $('#tt').tabs('getSelected');
			if (tab) {
				var index = $('#tt').tabs('getTabIndex', tab);
				$('#tt').tabs('close', index);
			}
		}
		<!--时间字段-->
		function showTime() {
			var date = new Date();
			$('#timeSpan').html();			
			$('#timeSpan').html(date.toLocaleString());
		}		
		setInterval(showTime, 1000);
		
		
		function logout(){
			window.location.href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/user/logout";
		}
	</script>
</body>

</html>