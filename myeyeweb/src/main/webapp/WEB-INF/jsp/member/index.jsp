<%@ page language="java" session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.freedom.monitor.utils.ConstantUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 网页布局参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/complextoolbar.html# -->
<!-- 添加一行，参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/rowediting.html -->
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/demo/demo.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/esf/esf.css">

<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.easyui.min.js"></script>

</head>
<body style="padding: 0px 0px 0px 0px; margin: 1px 1px 1px 1px;">

<!-- 新增窗口 结束-->
	<script type="text/javascript">
		var toolbar = [{
			text:'新增',
			iconCls:'icon-add',
			handler:function(){
				clearForm();
				$("#userid").val("");
				$('#newWindow').window('open');
			}
		},{
			text:'修改',
			iconCls:'icon-edit',
			handler:function(){
				clearForm();
				var row = $('#dg').datagrid('getSelected');  
	       		if (row){ 
	 				//把相关的值补上
	 				//$("#userid").val(row.userid);
	 				//$("#username").val(row.username);
	 				//$("#name").val(row.name);
	 				//$("#password").val(row.pwd);
	 				//$("#role").val(row.role);
	 				$('#ff').form('load',row);
	 				$('#newWindow').window('open');
	       		}	     		 
				
			}
		},'-',{
			text:'删除',
			iconCls:'icon-remove',
			handler:function(){destroy();}
		}];	
		</script>
		
	<table id="dg" title="" class="easyui-datagrid"
		style="width: 98%; height: auto"
		data-options="pagination:true,fitcolumns:true,rownumbers:true,singleSelect:true,url:'<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/member/query',method:'get',toolbar:toolbar,remoteSort:false,
                multiSort:true">
		<thead>
			<tr>
				<th data-options="field:'id',width:80">ID</th>
				<th data-options="field:'userid',width:120">用户编码</th>
				<th	data-options="field:'username',width:140,align:'center',editor:'textbox'">用户名</th>
				<th	data-options="field:'name',width:140,align:'center',editor:'textbox'">姓名</th>
				<th data-options="field:'pwd',width:80,align:'center'">密码</th>
				<th data-options="field:'role',width:0,align:'center'"></th>
				<th data-options="field:'roleName',width:140,align:'center'">角色</th>
				<th data-options="field:'createTime',width:150,align:'center'">创建时间</th>
				<th data-options="field:'token',width:200,align:'center'">Token</th>
			</tr>
		</thead>
	</table>
	
	
	
	
<!-- 新增/修改 窗口 开始 -->
	<div id="newWindow" class="easyui-window" title="编辑" data-options="modal:true,closed:true,minimizable:false,maximizable:false,iconCls:'icon-save'" style="display:none;width:500px;height:400px;padding:10px;">
	 <!-- 实际内容 -->	 
			<form id="ff" method="post">
			    <!-- 主键 -->
			    <div style="margin-bottom: 20px;display:none;">
					<input class="easyui-textbox" prompt="用户编码" name="userid" id="userid"
						style="width: 100%; height: 34px;"
						data-options="label:'用户编码',labelPosition:'left',required:false">
				</div>
				<!--  -->
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="用户名" name="username" id="username"
						style="width: 100%; height: 34px;"
						data-options="label:'用户名',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="姓名" name="name" id="name"
						style="width: 100%; height: 34px;"
						data-options="label:'姓名',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-passwordbox" prompt="密码" name="pwd" id="pwd"
						iconWidth="28" style="width: 100%; height: 34px; padding: 10px"
						data-options="label:'密码',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<select class="easyui-combobox" prompt="角色" name="role" id="role"
						style="width: 100%; height: 34px;"
						data-options="label:'角色',labelPosition:'left',required:true,editable:false">						   				
							<!-- <option value="4">普通管理员</option>	 -->			
							<option value="8">普通用户</option>	
					</select>
				</div>				
			</form>
			<div style="text-align: center; padding: 5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton"
					onclick="submitForm()" style="width: 80px">Submit</a> <a
					href="javascript:void(0)" class="easyui-linkbutton"
					onclick="clearForm()" style="width: 80px">Clear</a>
			</div>		
	</div>
	
	
	
	
	
	
	<script type="text/javascript">
	<!--隐藏某一列-->
	 $(function(){
		 $("#dg").datagrid("hideColumn", "role");
	 })
	//新增
	function submitForm(){
		var isValid = $("#ff").form('validate');
		if(false==isValid){
			return false;
		}
		//校验通过
		$.post('<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/member/addOrUpdate',
				{
			        userid   : $('#userid').val(),
					username : $('#username').val(),
					name     : $('#name').val(),
					pwd      : $('#pwd').val(),
					role     : $("#role").combobox("getValue")
				},
				function(result) {					
					$('#newWindow').window('close');//及时关闭
					if (result.success) {
						// 刷新用户数据  
						$('#dg')
								.datagrid(
										'reload');
					} else {
						$.messager
								.show({ // 显示错误信息  
									title : 'Error',
									msg : result.errorMsg
								});
					}
				}, 'json');
	}
	function clearForm() {
		$('#ff').form('clear');
	}	
	</script>
	
	<script type="text/javascript">			
		//删除用户
	    function destroy(){  
	        var row = $('#dg').datagrid('getSelected');  
	        if (row){  
	            $.messager.confirm('Confirm','确定要删除吗 ?',function(r){  
	                if (r){  
	                    $.post('<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/member/del',
														{
															userid : row.userid
														},
														function(result) {
															if (result.success) {
																// 刷新用户数据  
																$('#dg')
																		.datagrid(
																				'reload');
															} else {
																$.messager
																		.show({ // 显示错误信息  
																			title : 'Error',
																			msg : result.errorMsg
																		});
															}
														}, 'json');
									}//if(r)结束
								}//function(r)结束
	            );//$.messager.confirm结束
			}//if(row)结束
		}//destroy结束 
	</script>

	
</body>

</html>