package com.freedom.monitor.utils;

import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtils {
	public static void addCookie(HttpServletResponse response, String key, String value) {
		Cookie cookie = new Cookie(key, value);
		cookie.setMaxAge(ConstantUtils.COOKIE_MAX_AGE);
		cookie.setPath("/");
		response.addCookie(cookie);
	}

	public static void deleteCookie(HttpServletResponse response, String key){
		Cookie cookie = new Cookie(key, null);
		cookie.setMaxAge(0);//0表示立即删除
		cookie.setPath("/");
		response.addCookie(cookie);
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static HashMap getCookie(HttpServletRequest request) {
		HashMap kv = new HashMap();
		Cookie[] cookies = request.getCookies();
		if (null != cookies) {
			for (Cookie c : cookies) {
				kv.put(c.getName(), c.getValue());
			}
		}
		return kv;
	}
}
