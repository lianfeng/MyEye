package com.freedom.monitor.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.freedom.monitor.utils.ConstantUtils;

@WebFilter(filterName = "MyContextPathFilter", urlPatterns = { "/*" })
public class MyContextPathFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		// 在request中保留上下文路径,以后改的话只要改这1个地方就行了
		httpServletRequest.setAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH,
				request.getServletContext().getContextPath());
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
