package com.freedom.monitor.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.freedom.rpc.thrift.common.zookeeper.client.ClientSideZkReadyListener;

@WebListener
public class MyListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO,应该有更好的办法
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
				ClientSideZkReadyListener.ready();
		// }
		// }).start();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

}
