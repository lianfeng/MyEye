package com.freedom.monitor.myeye.client.report;

import com.freedom.monitor.myeye.client.utils.NumberUtils;

public class Statistics {
	private long totalCount = 0;
	private long totalSucceed = 0;
	private long totalCostTime = 0;
	private long maxCostTime = 0;
	private long minCostTime = 0;

	public Statistics(){
		totalCount = 0;
		totalSucceed = 0;
		totalCostTime = 0;
		maxCostTime = 0;
		minCostTime = 0;
	}
	public Statistics(Report report) {
		totalCount = 1;
		totalSucceed = report.getSucceed();
		totalCostTime = report.getCostTime();
		maxCostTime = report.getCostTime();
		minCostTime = report.getCostTime();
	}

	public void addReport(Report report) {
		totalCount += 1;
		totalSucceed += report.getSucceed();
		totalCostTime += report.getCostTime();
		maxCostTime = NumberUtils.max(maxCostTime, report.getCostTime());
		minCostTime = NumberUtils.min(minCostTime, report.getCostTime());
	}

	public void addStatistics(Statistics other) {
		totalCount += other.getTotalCount();
		totalSucceed += other.getTotalSucceed();
		totalCostTime += other.getTotalCostTime();
		maxCostTime = NumberUtils.max(maxCostTime, other.getMaxCostTime());
		minCostTime = NumberUtils.min(minCostTime, other.getMinCostTime());
	}

	public long getTotalCount() {
		return totalCount;
	}

	public long getTotalSucceed() {
		return totalSucceed;
	}

	public long getTotalCostTime() {
		return totalCostTime;
	}

	public long getMaxCostTime() {
		return maxCostTime;
	}

	public long getMinCostTime() {
		return minCostTime;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("totalCostTime: ").append(this.totalCostTime)//
				.append(" ").append("totalCount: ").append(this.totalCount)//
				.append(" ").append("totalSucceed: ").append(this.totalSucceed)//
				.append(" ").append("maxCostTime: ").append(this.maxCostTime)//
				.append(" ").append("minCostTime: ").append(this.minCostTime);
		return sb.toString();
	}
}
