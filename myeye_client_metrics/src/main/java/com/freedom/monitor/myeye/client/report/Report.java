package com.freedom.monitor.myeye.client.report;

public class Report {
	private String product="";
	private String service="";
	private String subKey="";
	private int succeed=-1;
	private int costTime=-1;

	//
	public String getProduct() {
		return product;
	}

	public Report setProduct(String product) {
		this.product = product;
		return this;
	}

	public String getService() {
		return service;
	}

	public Report setService(String service) {
		this.service = service;
		return this;
	}

	public String getSubKey() {
		return subKey;
	}

	public Report setSubKey(String subKey) {
		this.subKey = subKey;
		return this;
	}

	public int getSucceed() {
		return succeed;
	}

	public Report setSucceed(int succeed) {
		this.succeed = succeed;
		return this;
	}

	public int getCostTime() {
		return costTime;
	}

	public Report setCostTime(int costTime) {
		this.costTime = costTime;
		return this;
	}
}
