package com.freedom.monitor.myeye.client.utils;

import org.apache.log4j.Logger;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpUtils {
	//
	// http://square.github.io/okhttp/#examples
	//
	private static final Logger logger = Logger.getLogger(HttpUtils.class);
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

	public static String post(OkHttpClient client, String url, String json) throws Exception {
		// 构造url & 请求体
		RequestBody body = RequestBody.create(JSON, json);
		Request request = new Request.Builder().url(url).post(body).build();
		// 发射&拿回响应
		Response response = client.newCall(request).execute();
		return response.body().string();
	}
}
