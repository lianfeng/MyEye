package com.freedom.monitor.myeye.storage.utils;

import java.util.HashSet;
import java.util.Set;
import com.freedom.rpc.thrift.common.utils.Logger;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtils {
	private static final Logger logger = Logger.getLogger(RedisUtils.class);
	//
	private static JedisPool jedisPool = null;

	private static int getInt(String param, String defaultValue) {
		int result = Integer.parseInt(PropertyUtils.getInstance().getProperty(param, defaultValue));
		logger.info(param + " ---> " + result);
		return result;
	}

	private static String getString(String param, String defaultValue) {
		String result = PropertyUtils.getInstance().getProperty(param, defaultValue);
		logger.info(param + " ---> " + result);
		return result;
	}

	private static boolean getBoolean(String param) {
		boolean result = Boolean.valueOf(PropertyUtils.getInstance().getProperty(param));
		logger.info(param + " ---> " + result);
		return result;
	}

	static {
		//
		JedisPoolConfig config = new JedisPoolConfig();
		config.setMinIdle(getInt(ConstantUtils.REDIS_MIN_IDLE, "5"));
		config.setMaxTotal(getInt(ConstantUtils.REDIS_MAX_TOTAL, "30"));
		config.setBlockWhenExhausted(getBoolean(ConstantUtils.REDIS_BLOCK_WHEN_EXHAUSTED));
		config.setMaxWaitMillis(getInt(ConstantUtils.REDIS_MAX_WAIT_MILLIS, "1000"));
		config.setTestOnBorrow(getBoolean(ConstantUtils.REDIS_TEST_ON_BORROW));
		config.setTestOnCreate(getBoolean(ConstantUtils.REDIS_TEST_ON_CREATE));
		config.setMaxIdle(getInt(ConstantUtils.REDIS_MAX_IDLE, "30"));
		config.setTestOnReturn(getBoolean(ConstantUtils.REDIS_TEST_ON_RETURN));
		config.setTimeBetweenEvictionRunsMillis(getInt(ConstantUtils.REDIS_TIME_BETWEEN_EVICTION, "1000"));
		config.setMinEvictableIdleTimeMillis(getInt(ConstantUtils.REDIS_MIN_EVICTABLE_IDLE_TIME, "120000"));// 2分钟
		config.setTestWhileIdle(getBoolean(ConstantUtils.REDIS_TEST_WHILE_IDLE));
		config.setLifo(getBoolean(ConstantUtils.REDIS_LIFO));
		config.setNumTestsPerEvictionRun(getInt(ConstantUtils.REDIS_NUM_TEST, "6"));
		//
		// 初始化jedis连接池
		// TODO,后面有时间，会换成更高级的用法
		jedisPool = new JedisPool(config, getString(ConstantUtils.REDIS_IP, "127.0.0.1"),
				getInt(ConstantUtils.REDIS_PORT, "6379"), //
				getInt(ConstantUtils.REDIS_TIMEOUT, "2000"), //
				getString(ConstantUtils.REDIS_PASSWORD, "")//
		);
		//
		logger.info("succeed to create jedisPool ---> " + jedisPool);
	}

	public static void setWhenNotExist(Set<String> keys) {//可以失败
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			for (String str : keys) {
				jedis.set(str, "");// 永久保存
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			if (null != jedis) {
				jedis.close();// 释放jedis资源
			}
		}
	}

	// http://www.tuicool.com/articles/vaqABb
	public static Set<String> findNotExist(Set<String> originSet) {
		// 这里可以优化,可以做成一次性发送多条数据,但是也不能太多
		// TODO
		Set<String> newSet = new HashSet<String>();
		// 1)查询
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			// Transaction tx = jedis.multi();
			for (String str : originSet) {
				if (null == jedis.get(str)) {// 新出现的
					newSet.add(str);
				}
			}
			logger.debug("newSet ---> " + newSet);
			return newSet;
		} catch (Exception e) {
			logger.error(e.toString());
			return originSet;//如果有异常,比如redis挂了，则默认所有的都是新出现的
		} finally {
			if (null != jedis) {
				jedis.close();// 释放jedis资源
			}
		}
		//
	}

	public static void main(String[] args) {
		Set<String> p = new HashSet<String>();
		p.add("aaa");
		p.add("bbb");
		System.out.println(findNotExist(p));
	}
}
